// As a team, design how you would implement your program.

// 1. Write a program that
// expects a number N, and
// creates an array A with elements 0 - N
// now write a function that calculates the sum of the array
 
// 2. Write a program that
// expects a number N, and
// creates an array A with elements 0 - N,
// Now write a function that expects a number and an array and returns the index of that element.

//Documentation
//in Terminal, run `node 321.js`

const num = 10;

function fn32111 (N) {
    let a = [];
    for (let i = 0; i <= N; i++) {
        a[i] = i;
    };
    return a;
};

let arr = fn32111(num);
console.log(arr);

function fn32112 (arr) {
    const total = arr.reduce(
        (sum, value) => {
            return sum + value;
        }
    , 1);
    return total - 1;
};

console.log(fn32112(arr));