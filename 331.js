// As a team, design how you would implement your program.
// Actual implementation, to be done individually.

// 1. Write a function expect three parameters
// - number
// - successCallback
// - errorCallback
// 2. If the number entered is greater than 18 execute successCallback else execute errorCallback

//Documentation
//in Terminal, run `node 331.js`

//Promise version
let N = 16;

let fn331 = (number, successCallback, errorCallback) => {
    var promise = new Promise((resolve,reject)=>{
        console.log("Awaiting imaginary response in 3000ms...");
        setTimeout(resolve,3000);
    }).then(()=>{
        if (number > 18) {
            console.log(successCallback);
        } else {
            throw new Error(errorCallback);
        };
    }).catch((error)=>{
        console.log(error);
        console.log("Error caught");
    });
};

// Simpler?
// let N = 19;

// function fn331(number, successCallback, errorCallback) {
//     number > 18 ? successCallback : errorCallback
// };

fn331(N, "successCallback - N greater than 18", "errorCallback");