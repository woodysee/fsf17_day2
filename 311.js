// As a team, design how you would implement your program.
// Create a program that writes the lyrics to this song: http: //www.99-bottles-of-beer.net/lyrics.html
//     This is actually an American folk song.For a bit of a background: https: //en.wikipedia.org/wiki/99_Bottles_of_Beer.

//Documentation
//in Terminal, run `node 311.js`

function singBeerSong(){
    let a = [];
    let j = 0;
    for (let i = 99; i >= 0; i--) {
        switch (i) {
            case 1:
                a[j] = `${i} bottles of beer on the wall, ${i} bottles of beer.<br/>`;
                a[j] = a[j].toString().concat(`Take one down and pass it around, no more bottles of beer on the wall.<br/><br/>`);
                break;
            case 0:
                a[j] = `No more bottles of beer on the wall, no more bottles of beer.<br/>`;
                a[j] = a[j].toString().concat(`Go to the store and buy some more, 99 bottles of beer on the wall.<br/><br/>`);
                break;
            default:
                a[j] = `${i} bottles of beer on the wall, ${i} bottles of beer.<br/>`;
                a[j] = a[j].toString().concat(`Take one down and pass it around, ${i - 1} bottle of beer on the wall.<br/><br/>`);
                break;
        };
        j++;
    };
    return a;
};

console.log('------Beer Song still in Array-------')
console.log(singBeerSong());
// console.log('------Beer Song Paragraph with Breaks-------');
// console.log(singBeerSong().join(''));