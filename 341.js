// As a team, design how you would implement your program.
// Actual implementation, to be done individually.

// Create a Employee class with following
// - attributes
//     - firstName
//     - lastName
//     - employeeNumber
//     - salary
//     - age
//     - department
// - methods
//     - getFullName
//     - isRich - returns true if salary is greater than 2000
//     - belongsToDepartment - expects a string and checks if user is in department

//Documentation
//in Terminal, run `node 341.js`

class Employee {
    constructor(
        firstName,
        lastName,
        employeeNumber,
        salary, 
        age,
        department
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.employeeNumber = employeeNumber;
        this.salary = salary;
        this.age = age;
        this.department = department;
    }

    getFullName(){
        return this.firstName + " " + this.lastName;
    }

    isRich(){
        if (this.salary > 2000) {
            return true
        } else {
            return false
        }
    }

    belongsToDepartment(dep) {
        if (this.department == dep) {
            return this.firstName + " is in " + dep;
        } else {
            return this.firstName + " is NOT in " + dep;
        }
    }
};

let me = new Employee;
me.firstName = "Woody";
me.lastName = "See";
me.employeeNumber = "127312ygs";
me.salary = 1500;
me.age = 26;
me.department = "Web Development";

console.log(me);

console.log(me.getFullName() + " is rich: " + me.isRich() + ". Because I earn S$" + me.salary);

console.log(me.getFullName() + " belongs to web development? " + me.belongsToDepartment("Web Development"));
console.log(me.getFullName() + " belongs to marketing? " + me.belongsToDepartment("Marketing"));
